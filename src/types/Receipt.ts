import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

type Receipt ={
    id: number;
    craeteDate: Date;
    totalBefore: number;
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberDiscount: number;
    memberId: number;
    member?: Member;
    receiptItem?: ReceiptItem[]
} 

export { type Receipt}