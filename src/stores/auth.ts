import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {

    const currentUser = ref<User>({
        id: 1,
        email: 'user1@gmail.com',
        password: 'user1@password',
        fullName: 'มานาโซน งามเด่น',
        gender: 'male', //male,female,others
        roles: ['user'] //admin,user
    })

  return { currentUser }
})